class Code
  attr_reader :pegs

  PEGS = {
    :Red => 'R',
    :Green => 'G',
    :Blue => 'B',
    :Yellow => 'Y',
    :Orange => 'O',
    :Purple => 'P'
  }

  def initialize(pegs)
    @pegs = pegs
  end

  def [](idx)
      @pegs[idx]
    end

  def []=(pos, mark)
    @pegs[idx] = mark
  end

  def self.parse(string)
    valid = "RGBYOP"
    pegs = string.split("")
    raise Exception if pegs.any? { |p| !valid.include?(p.upcase) }
    Code.new(pegs)
  end

  def self.random
    valid = "RGBYOP"
    pegs = ""
    4.times {pegs += valid[rand(0..5)]}
    Code.new(pegs)
  end

  def exact_matches(guess)
    count = 0
    4.times { |i| count += 1 if guess.pegs[i].upcase == @pegs[i].upcase }

    winner if count == 4

    count
  end

  def winner
    puts "Congratulations, you guessed the secret code!"
  end

  def near_matches(guess)
    count = 0
    counted = ""
    @pegs = @pegs.split("") if @pegs.class == String
    4.times do |i|
      bool1 = @pegs.join("").upcase.include?(guess.pegs[i].upcase)
      bool2 = guess.pegs[i].upcase != @pegs[i].upcase
      bool3 = !counted.upcase.include?(guess.pegs[i].upcase)
      # bool4 = guess.pegs[i].upcase == @pegs[i].upcase

      if bool1 && bool2 && bool3
        count += 1
        counted += guess.pegs[i]
      # elsif bool3 && bool4
      #   counted += guess.pegs[i]
      end
    end
    count
  end

  def ==(guess)
    return false if guess.class != Code
    self.pegs.join("").upcase == guess.pegs.join("").upcase
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code
    @count = 0
  end

  def get_guess
    puts "Guess:"
    guess = gets.chomp
    puts ""
    @guess = Code.parse(guess)
  end

  def display_matches(guess)
    puts "exact matches: #{@secret_code.exact_matches(guess)}"
    puts "near matches: #{@secret_code.near_matches(guess)}"
    puts ""
  end

  def play
    puts @secret_code.pegs
    while @count < 12
      guess = get_guess
      display_matches(guess)
      @count += 1
      @count = 13 if @secret_code.pegs.join.downcase == guess.pegs.join.downcase
    end

    puts "Out of guesses, better luck next time." if @count == 12
  end
end

game = Game.new
game.play
